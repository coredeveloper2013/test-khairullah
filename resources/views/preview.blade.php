<html>
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=0.1">
	<title>images having width ({{$w}})</title>
</head>
<body style="margin: 0px; background: #0e0e0e;">
	<img width="{{$w}}" style="-webkit-user-select: none;" src="{{asset($media_data->url)}}">
</body>
</html>