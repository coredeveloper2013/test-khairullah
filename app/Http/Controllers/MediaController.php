<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use Validator;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'file' => 'required|mimes:jpeg,png,doc,docx,pdf|max:2048',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors);
        }

        $insert_array = [
                'title' => $request->input('title')
            ];

        if ($request->hasFile('file')) {
            $url = './public/media-images/';
            $urlname = 'public/media-images/';
            @mkdir($url, 0777, true);
            $r_files = $request->file('file');
            
            $imagename = time().mt_rand().'.'.$r_files->getClientOriginalExtension();
            $r_files->move($url, $imagename);

            $insert_array['filename']= $imagename;
            $insert_array['url']= $urlname.$imagename;
            $insert_array['actual_name']= $r_files->getClientOriginalName();
            $insert_array['ext']= $r_files->getClientOriginalExtension();
            $insert_array['size']= $_FILES['file']['size'];
            $insert_array['mimetype']= $r_files->getClientMimeType();
        }
        if(Media::create($insert_array)){
            return response()->json(['message' => 'Media Saved Successfully']);
        }else{
            return response()->json(['message' => 'Error Saving Media']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // dd($request->all());
        if($request->input('img') && $request->input('w')){
            $data['media_data'] = Media::where('filename',$request->input('img'))->first();
            if($data['media_data']){
                $data['w'] = $request->input('w');
                return view('preview',$data);
            }else{
                return response()->json(['message' => 'Media not found']);
            }
        }else{
            return response()->json(['message' => 'Please give image name and width']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->input('id')){
            foreach ($request->input('id') as $id_value) {
                $media_data = Media::where('id',$id_value)->first();
                if($media_data){
                    @unlink($media_data->url);
                    Media::where('id',$id_value)->delete();
                }
            }
        }else{
            return response()->json(['message' => 'Please select at least one media']);
        }

        return response()->json(['message' => 'Selected media has been deleted successfully']);
    }
}
